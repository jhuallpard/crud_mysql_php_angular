
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Headers: *");
if ($_SERVER["REQUEST_METHOD"] != "PUT") {
    exit("Solo acepto peticiones PUT");
}
$jsonPositions = json_decode(file_get_contents("php://input"));
if (!$jsonPositions) {
    exit("No hay datos");
}
$bd = include_once "bd.php";
$sentencia = $bd->prepare("UPDATE positions SET name = ?, description = ?, status = ?, created = ?, modified = ? WHERE id = ?");
$resultado = $sentencia->execute([$jsonPositions->name, $jsonPositions->description, $jsonPositions->status, $jsonPositions->created, $jsonPositions->modified,  $jsonPositions->id]);
echo json_encode($resultado);

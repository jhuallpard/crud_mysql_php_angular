<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Headers: *");
$jsonMenus = json_decode(file_get_contents("php://input"));
if (!$jsonMenus) {
    exit("No hay datos");
}
$bd = include_once "bd.php";
$sentencia = $bd->prepare("insert into menus(title, description, status, created, modified, father_id, link, position_id) values (?,?,?,?,?,?,?,?)");
$resultado = $sentencia->execute([$jsonMenus->title, $jsonMenus->description, $jsonMenus->status, $jsonMenus->created, $jsonMenus->modified, $jsonMenus->father_id, $jsonMenus->link, $jsonMenus->position_id]);
echo json_encode([
    "resultado" => $resultado,
]);

<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
if (empty($_GET["idMenus"])) {
    exit("No hay id de menus");
}
$idMenus = $_GET["idMenus"];
$bd = include_once "bd.php";
$sentencia = $bd->prepare("select id, title, description, status, created, modified, father_id, link, position_id from menus where id = ?");
$sentencia->execute([$idMenus]);
$menus = $sentencia->fetchObject();
echo json_encode($menus);

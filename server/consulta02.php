<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
$bd = include_once "bd.php";
$sentencia01 = $bd->query("SELECT menus.id, menus.position_id, menus.title, menus.father_id, menus.description, positions.description, menus.link
		FROM menus 
		JOIN positions ON menus.position_id = positions.id
        WHERE menus.status = 1 and menus.father_id is not NULL");
$consulta01 = $sentencia01->fetchAll(PDO::FETCH_OBJ);
echo json_encode($consulta01);
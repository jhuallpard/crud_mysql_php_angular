
<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: DELETE");
$metodo = $_SERVER["REQUEST_METHOD"];
if ($metodo != "DELETE" && $metodo != "OPTIONS") {
    exit("Solo se permite método DELETE");
}

if (empty($_GET["idPositions"])) {
    exit("No hay id de positions para eliminar");
}
$idPositions = $_GET["idPositions"];
$bd = include_once "bd.php";
$sentencia = $bd->prepare("DELETE FROM positions WHERE id = ?");
$resultado = $sentencia->execute([$idPositions]);
echo json_encode($resultado);

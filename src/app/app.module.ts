import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AgregarMenusComponent } from 'src/app/components/menus/agregar-menus/agregar-menus.component';
import { EditarMenusComponent } from 'src/app/components/menus/editar-menus/editar-menus.component';
import { ListarMenusComponent } from 'src/app/components/menus/listar-menus/listar-menus.component';
import { AgregarPositionsComponent } from 'src/app/components/positions/agregar-positions/agregar-positions.component';
import { EditarPositionsComponent } from 'src/app/components/positions/editar-positions/editar-positions.component';
import { ListarPositionsComponent } from 'src/app/components/positions/listar-positions/listar-positions.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { DialogoConfirmacionComponent } from 'src/app/components/dialogo-confirmacion/dialogo-confirmacion.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AcercaDeComponent } from './acerca-de/acerca-de.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ConsultaMenuComponent } from './components/consulta-menu/consulta-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    AgregarMenusComponent,
    ListarMenusComponent,
    EditarMenusComponent,
    AgregarPositionsComponent,
    ListarPositionsComponent,
    EditarPositionsComponent,
    DialogoConfirmacionComponent,
    AcercaDeComponent,
    NavbarComponent,
    ConsultaMenuComponent
  ],
  entryComponents: [
    DialogoConfirmacionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatTableModule,
    MatDialogModule,
    MatSnackBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


import { Component, OnInit } from '@angular/core';
import { ConsultaService } from "src/app/services/consulta.service"
import { Consulta } from "src/app/models/consulta"
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from "src/app/components/dialogo-confirmacion/dialogo-confirmacion.component"
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-consulta-menu',
  templateUrl: './consulta-menu.component.html',
  styleUrls: ['./consulta-menu.component.css']
})
export class ConsultaMenuComponent implements OnInit {
  public consulta: Consulta[] = [
    //new Menus("primary", "tipo primario", 1, new Date(), new Date(), 1, "hola mundo", 1)
  ];

  constructor(private consultaService: ConsultaService, private dialogo: MatDialog, private snackBar: MatSnackBar) { }


  ngOnInit() {
    this.obtenerConsulta();
  }

  obtenerConsulta() {
    return this.consultaService
      .getConsulta()
      .subscribe((consulta: Consulta[]) => this.consulta = consulta);
  }

}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaMenuComponent } from './consulta-menu.component';

describe('ConsultaMenuComponent', () => {
  let component: ConsultaMenuComponent;
  let fixture: ComponentFixture<ConsultaMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

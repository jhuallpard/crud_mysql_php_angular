import { Component, OnInit } from '@angular/core';
import { MenusService } from "src/app/services/menus.service"
import { Menus } from "src/app/models/menus"
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from "src/app/components/dialogo-confirmacion/dialogo-confirmacion.component"
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-listar-menus',
  templateUrl: './listar-menus.component.html',
  styleUrls: ['./listar-menus.component.css']
})
export class ListarMenusComponent implements OnInit {
  public menus: Menus[] = [
    new Menus("primary", "tipo primario", 1, new Date(), new Date(), 1, "hola mundo", 1)
  ];

  constructor(private menusService: MenusService, private dialogo: MatDialog, private snackBar: MatSnackBar) { }

  eliminarMenus(menus: Menus) {
    this.dialogo
      .open(DialogoConfirmacionComponent, {
        data: `¿Realmente quieres eliminar a ${menus.title}?`
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (!confirmado) return;
        this.menusService
          .deleteMenu(menus)
          .subscribe(() => {
            this.obtenerMenus();
            this.snackBar.open('Menus eliminada', undefined, {
              duration: 1500,
            });
          });
      })
  }

  ngOnInit() {
    this.obtenerMenus();
  }

  obtenerMenus() {
    return this.menusService
      .getMenus()
      .subscribe((menus: Menus[]) => this.menus = menus);
  }

}

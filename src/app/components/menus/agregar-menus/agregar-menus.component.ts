
import { Component, OnInit } from '@angular/core';
import { Menus } from 'src/app/models/menus';
import { MenusService } from "src/app/services/menus.service"
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-agregar-menus',
  templateUrl: './agregar-menus.component.html',
  styleUrls: ['./agregar-menus.component.css']
})
export class AgregarMenusComponent implements OnInit {

  constructor(private menusService: MenusService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit() {
  }
  menusModel = new Menus("", "", undefined, new Date(),new Date(), undefined, "", undefined)

  onSubmit() {
    this.menusService.addMenu(this.menusModel).subscribe(() => {
      this.snackBar.open('Menu guardada', undefined, {
        duration: 1500,
      });
      this.router.navigate(['/menus']);
    })
  }

}

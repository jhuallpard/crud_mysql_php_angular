
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenusService } from "src/app/services/menus.service"
import { Menus } from 'src/app/models/menus';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-editar-menus',
  templateUrl: './editar-menus.component.html',
  styleUrls: ['./editar-menus.component.css']
})
export class EditarMenusComponent implements OnInit {

  public menus: Menus = new Menus("", "", 0, new Date(),new Date(), 0, "", 0);

  constructor(private route: ActivatedRoute,
    private router: Router, private menusService: MenusService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    let idMenu = this.route.snapshot.paramMap.get("id");
    this.menusService.getMenu(idMenu).subscribe((menus: Menus) => this.menus = menus)
  }

  volver() {
    this.router.navigate(['/menus']);
  }

  onSubmit() {
    this.menusService.updateMenu(this.menus).subscribe(() => {
      this.snackBar.open('Menu actualizado', undefined, {
        duration: 1500,
      });
      this.volver();
    });
  }

}

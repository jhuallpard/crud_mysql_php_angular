
import { Component, OnInit } from '@angular/core';
import { Positions } from 'src/app/models/positions';
import { PositionsService } from "src/app/services/positions.service"
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-agregar-positions',
  templateUrl: './agregar-positions.component.html',
  styleUrls: ['./agregar-positions.component.css']
})
export class AgregarPositionsComponent implements OnInit {

  constructor(private positionsService: PositionsService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit() {
  }
  positionsModel = new Positions("", "", undefined, new Date(),new Date())

  onSubmit() {
    this.positionsService.addPosition(this.positionsModel).subscribe(() => {
      this.snackBar.open('Position guardada', undefined, {
        duration: 1500,
      });
      this.router.navigate(['/positions']);
    })
  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarPositionsComponent } from './agregar-positions.component';

describe('AgregarMascotaComponent', () => {
  let component: AgregarPositionsComponent;
  let fixture: ComponentFixture<AgregarPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PositionsService } from "src/app/services/positions.service"
import { Positions } from 'src/app/models/positions';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-editar-positions',
  templateUrl: './editar-positions.component.html',
  styleUrls: ['./editar-positions.component.css']
})
export class EditarPositionsComponent implements OnInit {

  public positions: Positions = new Positions("", "", 0, new Date(),new Date());

  constructor(private route: ActivatedRoute,
    private router: Router, private positionsService: PositionsService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    let idPosition = this.route.snapshot.paramMap.get("id");
    this.positionsService.getPosition(idPosition).subscribe((positions: Positions) => this.positions = positions)
  }

  volver() {
    this.router.navigate(['/positions']);
  }

  onSubmit() {
    this.positionsService.updatePosition(this.positions).subscribe(() => {
      this.snackBar.open('Position actualizado', undefined, {
        duration: 1500,
      });
      this.volver();
    });
  }

}

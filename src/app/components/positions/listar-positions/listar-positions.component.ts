import { Component, OnInit } from '@angular/core';
import { PositionsService } from "src/app/services/positions.service"
import { Positions } from "src/app/models/positions"
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from "src/app/components/dialogo-confirmacion/dialogo-confirmacion.component"
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-listar-positions',
  templateUrl: './listar-positions.component.html',
  styleUrls: ['./listar-positions.component.css']
})
export class ListarPositionsComponent implements OnInit {
  public positions: Positions[] = [
    new Positions("primary", "tipo primario", 1, new Date(), new Date())
  ];

  constructor(private positionsService: PositionsService, private dialogo: MatDialog, private snackBar: MatSnackBar) { }

  eliminarPositions(positions: Positions) {
    this.dialogo
      .open(DialogoConfirmacionComponent, {
        data: `¿Realmente quieres eliminar a ${positions.name}?`
      })
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (!confirmado) return;
        this.positionsService
          .deletePosition(positions)
          .subscribe(() => {
            this.obtenerPositions();
            this.snackBar.open('Positions eliminada', undefined, {
              duration: 1500,
            });
          });
      })
  }

  ngOnInit() {
    this.obtenerPositions();
  }

  obtenerPositions() {
    return this.positionsService
      .getPositions()
      .subscribe((positions: Positions[]) => this.positions = positions);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Positions } from "src/app/models/positions"
import { environment } from "src/environments/environment"
@Injectable({
  providedIn: 'root'
})
export class PositionsService {
  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) { }

  getPositions() {
    return this.http.get(`${this.baseUrl}/getAll_positions.php`);
  }

  getPosition(id: string | number) {
    return this.http.get(`${this.baseUrl}/get_positions.php?idPositions=${id}`);
  }

  addPosition(positions: Positions) {
    return this.http.post(`${this.baseUrl}/post_positions.php`, positions);
  }

  deletePosition(positions: Positions) {
    return this.http.delete(`${this.baseUrl}/delete_positions.php?idPositions=${positions.id}`);
  }

  updatePosition(positions: Positions) {
    return this.http.put(`${this.baseUrl}/update_positions.php`, positions);
  }
}

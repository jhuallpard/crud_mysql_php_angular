import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Consulta } from "src/app/models/consulta"
import { environment } from "src/environments/environment"
@Injectable({
  providedIn: 'root'
})
export class ConsultaService {
  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) { }

  getConsulta() {
    return this.http.get(`${this.baseUrl}/consulta.php`);
  }

  getConsulta02() {
    return this.http.get(`${this.baseUrl}/consulta02.php`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Menus } from "src/app/models/menus"
import { environment } from "src/environments/environment"
@Injectable({
  providedIn: 'root'
})
export class MenusService {
  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) { }

  getMenus() {
    return this.http.get(`${this.baseUrl}/getAll_menus.php`);
  }

  getMenu(id: string | number) {
    return this.http.get(`${this.baseUrl}/get_menus.php?idMenus=${id}`);
  }

  addMenu(menus: Menus) {
    return this.http.post(`${this.baseUrl}/post_menus.php`, menus);
  }

  deleteMenu(menus: Menus) {
    return this.http.delete(`${this.baseUrl}/delete_menus.php?idMenus=${menus.id}`);
  }

  updateMenu(menus: Menus) {
    return this.http.put(`${this.baseUrl}/update_menus.php`, menus);
  }
}


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarMenusComponent } from 'src/app/components/menus/agregar-menus/agregar-menus.component';
import { ListarMenusComponent } from 'src/app/components/menus/listar-menus/listar-menus.component';
import { EditarMenusComponent } from 'src/app/components/menus/editar-menus/editar-menus.component';
import { AgregarPositionsComponent } from 'src/app/components/positions/agregar-positions/agregar-positions.component';
import { ListarPositionsComponent } from 'src/app/components/positions/listar-positions/listar-positions.component';
import { EditarPositionsComponent } from 'src/app/components/positions/editar-positions/editar-positions.component';
import { AcercaDeComponent } from './acerca-de/acerca-de.component';
import { ConsultaMenuComponent } from './components/consulta-menu/consulta-menu.component';

const routes: Routes = [
  { path: "acerca-de", component: AcercaDeComponent },
  { path: "menus", component: ListarMenusComponent },
  { path: "menus/agregar", component: AgregarMenusComponent },
  { path: "menus/editar/:id", component: EditarMenusComponent },
  { path: "positions", component: ListarPositionsComponent },
  { path: "positions/agregar", component: AgregarPositionsComponent },
  { path: "positions/editar/:id", component: EditarPositionsComponent },
  { path: "consulta", component: ConsultaMenuComponent },

  { path: "", redirectTo: "/menus", pathMatch: "full" },// Cuando es la raíz
  { path: "**", redirectTo: "/menus" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

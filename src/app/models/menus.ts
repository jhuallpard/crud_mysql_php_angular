export class Menus {
    constructor(
        public title: string,
        public description: string,
        public status: number,
        public created: Date,
        public modified: Date,
        public father_id: number,
        public link: string,
        public position_id: number,
        public id?: number,
    ) { }

}

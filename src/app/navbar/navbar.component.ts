import { Component, OnInit } from '@angular/core';
import { ConsultaService } from "src/app/services/consulta.service"
import { Consulta } from "src/app/models/consulta"
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from "src/app/components/dialogo-confirmacion/dialogo-confirmacion.component"
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
	 public consulta: Consulta[] = [
	    //new Menus("primary", "tipo primario", 1, new Date(), new Date(), 1, "hola mundo", 1)
	  ];

	  public consulta02: Consulta[] = [
	    //new Menus("primary", "tipo primario", 1, new Date(), new Date(), 1, "hola mundo", 1)
	  ];

  constructor(private consultaService: ConsultaService, private dialogo: MatDialog, private snackBar: MatSnackBar) { }


  ngOnInit() {
    this.obtenerConsulta();
    this.obtenerConsulta02();
  }

  obtenerConsulta() {
    return this.consultaService
      .getConsulta()
      .subscribe((consulta: Consulta[]) => this.consulta = consulta);
  }

  obtenerConsulta02() {
    return this.consultaService
      .getConsulta02()
      .subscribe((consulta02: Consulta[]) => this.consulta02 = consulta02);
  }

}